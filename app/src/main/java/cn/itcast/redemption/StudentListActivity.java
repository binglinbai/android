package cn.itcast.redemption;

import androidx.annotation.NonNull;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//StudentListActivity 学生列表
public class StudentListActivity extends ListActivity implements DialogInterface.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, View.OnClickListener {
    private static final String TAG = "TestSQLite";
    private Button addStudent;
    private Cursor cursor;
    private SimpleCursorAdapter adapter;
    private ListView listView;
    private List<Long> list;
    private RelativeLayout relativeLayout;
    private Button searchButton;
    private Button selectButton;
    private Button deleteButton;
    private Button selectAllButton;
    private Button canleButton;
    private Button orderButton;
    private LinearLayout layout;
    private StudentDao dao;
    private Student student;
    private Boolean isDeleteList = false;
    private Menu menu;
    private CheckBox box;
    private CheckBox cbbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG, "onCreate");
        list = new ArrayList<Long>();
        Student student = new Student();
        dao = new StudentDao(new StudentDBHelper(this));
        orderButton = (Button) findViewById(R.id.bn_order);
        addStudent = (Button) findViewById(R.id.btn_add_student);
        searchButton = (Button) findViewById(R.id.bn_search_id);
        selectButton = (Button) findViewById(R.id.bn_select);
        deleteButton = (Button) findViewById(R.id.bn_delete);
        selectAllButton = (Button) findViewById(R.id.bn_selectall);
        canleButton = (Button) findViewById(R.id.bn_canel);
        layout = (LinearLayout) findViewById(R.id.showLiner);
        relativeLayout = (RelativeLayout) findViewById(R.id.RelativeLayout);
        listView = getListView();


        //位按钮设置监听事件
        addStudent.setOnClickListener(this);
        searchButton.setOnClickListener(this);
        selectButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        canleButton.setOnClickListener(this);
        orderButton.setOnClickListener(this);
        selectAllButton.setOnClickListener(this);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        listView.setOnCreateContextMenuListener(this);
        // cbbox = findViewById(R.id.cb_box);

        load();
        registerForContextMenu(orderButton);//控件注册


    }

    @Override
    public void onClick(DialogInterface dialog, int which) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_student:
                startActivity(new Intent(StudentListActivity.this, AddStudentActivity.class));
                break;
            case R.id.bn_search_id:
                startActivity(new Intent(this, StudentSearch.class));
                break;
            case R.id.bn_select:
                //问题就出在这
                //这是一个更多选项的逻辑
                layout.setVisibility(View.VISIBLE);
                // =！如果为真取假，如果为假取真
                isDeleteList = !isDeleteList;
                if (isDeleteList) {
                    checkOrClearAllCheckboxs(true);
                } else {
                    showOrHiddenCheckBoxs(false);
                }

                break;

            //此处也是删除重要逻辑之一
            case R.id.bn_delete:
                //删除按钮
                //如果表内数据大于0条
                if (list.size() > 0) {
                    //定义一个小于表长的i，执行自增
                    for (int i = 0; i < list.size(); i++) {
                        //定义一个长整型id，等于表中的第i条数据
                        long id = list.get(i);
                        //打印出通知，删除id为？的数据
                        Log.e(TAG, "delete id=" + id);
                        //定义一个count，删除数据库中id为？的数据
                        int count = dao.deleteStudentById(id);

                    }
                    dao.closeDB();
                    cancel();
                    load();
                }
                break;
            //取消按钮
            case R.id.bn_canel:


                cancel();
                load();
                break;

            //关于按钮
            case R.id.bn_selectall:
                Intent intent = new Intent();
                intent.setClass(this, About.class);
                this.startActivity(intent);

//                //删除按钮
//                //如果表内数据大于0条
//                if(list.size()>0){
//                    for (int i=0;i<list.size();i++){
//                        dao.deleteStudentById(i);
//                    }
//                }
//                else {
//                    dao.closeDB();
//                    load();
//                }
                break;
            //排序按钮
            case R.id.bn_order:

                v.showContextMenu();//单击直接显示Context菜单
                break;
        }
    }

    //长按事件的结果 出现菜单
    @Override

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.menu, menu);

    }

    public void cancel() {
        isDeleteList = !isDeleteList;
        layout.setVisibility(View.GONE);
        int childCount = listView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = listView.getChildAt(i);
            if (view != null) {
                CheckBox box = (CheckBox) view.findViewById(R.id.cb_box);
                int invisible = View.INVISIBLE;
                box.setVisibility(invisible);
            }
        }

    }


    //对菜单中的按钮添加点击事件
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        student = (Student) listView.getTag();
        Intent intent = new Intent();
        StudentDBHelper studentDBHelper = new StudentDBHelper(
                StudentListActivity.this);
        SQLiteDatabase database = studentDBHelper.getWritableDatabase();
        switch (item_id) {
            case R.id.cancel:
                load();
                break;
            case R.id.below60:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "ch", "SUM(ch)<60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.btw6079:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "ch", "SUM(ch)<80 and SUM(ch)>=60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.btw8089:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "ch", "SUM(ch)<90 and SUM(ch)>=79", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.high90:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "ch", "SUM(ch)>=90", null, null);
                order(cursor);
                dao.closeDB();
                break;


            case R.id.dabelow60:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "da", "SUM(da)<60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.dabtw6079:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "da", "SUM(da)<80 and SUM(da)>=60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.dabtw8089:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "da", "SUM(da)<90 and SUM(da)>=79", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.dahigh90:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "da", "SUM(da)>=90", null, null);
                order(cursor);
                dao.closeDB();
                break;


            case R.id.enbelow60:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "en", "SUM(en)<60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.enbtw6079:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "en", "SUM(en)<80 and SUM(en)>=60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.enbtw8089:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "en", "SUM(en)<90 and SUM(en)>=79", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.enhigh90:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "en", "SUM(en)>=90", null, null);
                order(cursor);
                dao.closeDB();
                break;


            case R.id.combelow60:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "comp", "SUM(comp)<60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.combtw6079:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "comp", "SUM(comp)<80 and SUM(comp)>=60", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.combtw8089:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "comp", "SUM(comp)<90 and SUM(comp)>=79", null, null);
                order(cursor);
                dao.closeDB();
                break;
            case R.id.comhigh90:
                cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null, "comp", "SUM(comp)>=90", null, null);
                order(cursor);
                dao.closeDB();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


        Student student = (Student) dao.getStudentFromView(view, id);
        listView.setTag(student);
        registerForContextMenu(listView);
        return false;
    }

    // 点击一条记录是触发的事件
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if (!isDeleteList) {
            student = dao.getStudentFromView(view, id);
            Log.e(TAG, "student*****" + dao.getStudentFromView(view, id));
            Intent intent = new Intent();
            intent.putExtra("student", student);
            intent.setClass(this, ShowStudentActivity.class);
            this.startActivity(intent);
        } else {
            //删除时勾选框逻辑之一
            //主要功能是在选择模式下点击记录的事件
            CheckBox box = (CheckBox) view.findViewById(R.id.cb_box);
            box.setChecked(!box.isChecked());
            list.add(id);
            deleteButton.setEnabled(box.isChecked());
        }
    }

    //排序功能中把数据加载出来的方法
    public void order(Cursor cursor) {
        startManagingCursor(cursor);
        adapter = new SimpleCursorAdapter(this, R.layout.student_list_item,
                cursor, new String[]{TableContanst.StudentColumns.ID,
                TableContanst.StudentColumns.NAME,
                TableContanst.StudentColumns.AGE,
                TableContanst.StudentColumns.SEX,
                TableContanst.StudentColumns.LIKES,
                TableContanst.StudentColumns.CH,
                TableContanst.StudentColumns.DA,
                TableContanst.StudentColumns.EN,
                TableContanst.StudentColumns.COMP,
                TableContanst.StudentColumns.PHONE_NUMBER,
                TableContanst.StudentColumns.TRAIN_DATE}, new int[]{
                R.id.tv_stu_id, R.id.tv_stu_name, R.id.tv_stu_age,
                R.id.tv_stu_sex, R.id.tv_stu_likes, R.id.tv_stu_ch, R.id.tv_stu_da, R.id.tv_stu_en, R.id.tv_stu_comp, R.id.tv_stu_phone,
                R.id.tv_stu_traindate});
        listView.setAdapter(adapter);

    }

    // 自定义一个加载数据库中的全部记录到当前页面的无参方法
    public void load() {
        StudentDBHelper studentDBHelper = new StudentDBHelper(
                StudentListActivity.this);
        SQLiteDatabase database = studentDBHelper.getWritableDatabase();
        cursor = database.query(TableContanst.STUDENT_TABLE, null, null, null,
                null, null, TableContanst.StudentColumns.ID + " desc");
        startManagingCursor(cursor);
        adapter = new SimpleCursorAdapter(this, R.layout.student_list_item,
                cursor, new String[]{TableContanst.StudentColumns.ID,
                TableContanst.StudentColumns.NAME,
                TableContanst.StudentColumns.AGE,
                TableContanst.StudentColumns.SEX,
                TableContanst.StudentColumns.LIKES,
                TableContanst.StudentColumns.CH,
                TableContanst.StudentColumns.DA,
                TableContanst.StudentColumns.EN,
                TableContanst.StudentColumns.COMP,
                TableContanst.StudentColumns.PHONE_NUMBER,
                TableContanst.StudentColumns.TRAIN_DATE}, new int[]{
                R.id.tv_stu_id, R.id.tv_stu_name, R.id.tv_stu_age,
                R.id.tv_stu_sex, R.id.tv_stu_likes, R.id.tv_stu_ch, R.id.tv_stu_da, R.id.tv_stu_en, R.id.tv_stu_comp, R.id.tv_stu_phone,
                R.id.tv_stu_traindate});
        listView.setAdapter(adapter);
    }

    // 勾选框全选或全取消
    private void checkOrClearAllCheckboxs(boolean b) {

        int childCount = listView.getChildCount();
        Log.e(TAG, "list child size=" + childCount);
        for (int i = 0; i < childCount; i++) {
            View view = listView.getChildAt(i);
            if (view != null) {
                CheckBox box = (CheckBox) view.findViewById(R.id.cb_box);
                box.setChecked(!b);
            }
        }
        showOrHiddenCheckBoxs(true);
    }

    // 显示或者隐藏自定义菜单
    private void showOrHiddenCheckBoxs(boolean b) {
        int childCount = listView.getChildCount();
        Log.e(TAG, "list child size=" + childCount);
        for (int i = 0; i < childCount; i++) {
            View view = listView.getChildAt(i);
            if (view != null) {
                final CheckBox box = (CheckBox) view.findViewById(R.id.cb_box);
                int visible = b ? View.VISIBLE : View.GONE;
                box.setVisibility(visible);
                final int finalI = i;
                box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        long id = listView.getItemIdAtPosition(finalI);
                        Log.e("test", id + "");
                        //box.setChecked(!box.isChecked());
                        list.add(id);
                        deleteButton.setEnabled(box.isChecked());
                    }
                });
                layout.setVisibility(visible);
                deleteButton.setEnabled(false);
            }
        }
    }

    // 自定义一个利用对话框形式进行数据的删除

    private void deleteStudentInformation(final long delete_id) {
        // 利用对话框的形式删除数据
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("学员信息删除")
                .setMessage("确定删除所选记录?")
                .setCancelable(false)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int raws = dao.deleteStudentById(delete_id);
                        layout.setVisibility(View.GONE);
                        isDeleteList = !isDeleteList;
                        load();
                        if (raws > 0) {
                            Toast.makeText(StudentListActivity.this, "删除成功!",
                                    Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(StudentListActivity.this, "删除失败!",
                                    Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // 点击全选事件时所触发的响应
    private void selectAllMethods() {
        // 全选，如果当前全选按钮显示是全选，则在点击后变为取消全选，如果当前为取消全选，则在点击后变为全选
        if (selectAllButton.getText().toString().equals("全选")) {
            int childCount = listView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View view = listView.getChildAt(i);
                if (view != null) {
                    CheckBox box = (CheckBox) view.findViewById(R.id.cb_box);
                    box.setChecked(true);
                    deleteButton.setEnabled(true);
                    selectAllButton.setText("取消全选");
                }
            }
        } else if (selectAllButton.getText().toString().equals("取消全选")) {
            checkOrClearAllCheckboxs(true);
            deleteButton.setEnabled(false);
            selectAllButton.setText("全选");
        }
    }

}
