package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StuCheck extends AppCompatActivity {
    private ListView listView;
    private SimpleCursorAdapter adapter;
    private List<Long> list;
    private Cursor cursor;
    private StudentDBHelper studentDBHelper;
    private ListView list1;
    private TextView tishi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stu_check);
        setTitle("查成绩页面");
        list = new ArrayList<Long>();
        list1 = findViewById(R.id.list1);
        load();
        Intent intent = getIntent();
        String zhanghao = intent.getStringExtra("xuehao");
        tishi = findViewById(R.id.tishi);
        tishi.setText("学号--" + zhanghao + "，您的成绩如下：");


    }

    public void load() {
        Intent intent = getIntent();
        String zhanghao = intent.getStringExtra("xuehao");
        StudentDBHelper studentDBHelper = new StudentDBHelper(
                StuCheck.this);
        SQLiteDatabase database = studentDBHelper.getWritableDatabase();
        cursor = database.query(TableContanst.STUDENT_TABLE, null, "phone_number=?", new String[]{zhanghao},
                null, null, null);
        startManagingCursor(cursor);
        adapter = new SimpleCursorAdapter(this, R.layout.studentchecklist,
                cursor, new String[]{
                TableContanst.StudentColumns.CH,
                TableContanst.StudentColumns.DA,
                TableContanst.StudentColumns.EN,
                TableContanst.StudentColumns.COMP,}, new int[]{
                R.id.tv_stu_ch, R.id.tv_stu_da, R.id.tv_stu_en, R.id.tv_stu_comp
        });
        list1.setAdapter(adapter);
    }
}
