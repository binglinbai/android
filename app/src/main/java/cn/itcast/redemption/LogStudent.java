package cn.itcast.redemption;

public class LogStudent {
    private String xuehao;
    private String mima;

    public LogStudent(String xuehao, String mima) {
        this.xuehao = xuehao;
        this.mima = mima;
    }

    public String getXuehao() {
        return xuehao;
    }

    public String getMima() {
        return mima;
    }

    public void setXuehao(String xuehao) {
        this.xuehao = xuehao;
    }

    public void setMima(String mima) {
        this.mima = mima;
    }
}
