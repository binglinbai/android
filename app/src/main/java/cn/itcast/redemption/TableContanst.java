package cn.itcast.redemption;

public final class TableContanst {
    public static final String STUDENT_TABLE = "student";

    public static final class StudentColumns {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String AGE = "age";
        public static final String SEX = "sex";
        public static final String LIKES = "likes";
        public static final String PHONE_NUMBER = "phone_number";
        public static final String TRAIN_DATE = "train_date";
        public static final String MODIFY_TIME = "modify_time";
        //新增定义
        public static final String CH = "ch";
        public static final String DA = "da";
        public static final String EN = "en";
        public static final String COMP = "comp";
    }

}
