package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Forgot extends AppCompatActivity {
    private Button button;
    private Button cancel;
    private Button stucha;
    private EditText change1;
    private EditText change2;
    private EditText change3;
    private LogStuDBHelper logStuDBHelper;
    private LogTeaDBHelper logTeaDBHelper;
    private Button teacha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot);
        logStuDBHelper = new LogStuDBHelper(this);
        logTeaDBHelper = new LogTeaDBHelper(this);
        change1 = findViewById(R.id.et2_name);
        change2 = findViewById(R.id.et2_password);
        change3 = findViewById(R.id.et2_repassword);
        setTitle("密码修改");
        //取消按钮
        cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
        //学生修改按钮
        stucha = findViewById(R.id.btn_stulog);
        stucha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b1 = change1.getText().toString().trim();
                String b2 = change2.getText().toString().trim();
                String b3 = change3.getText().toString().trim();
                if (!TextUtils.isEmpty(b1) && !TextUtils.isEmpty(b2)) {
                    if (!b2.equals(b3)) {
                        Toast.makeText(Forgot.this, "两次输入密码不一致！", Toast.LENGTH_SHORT).show();
                    } else {
                        int a = logStuDBHelper.update(change1.getText().toString(), change2.getText().toString());
                        if (a > 0) {
                            Toast.makeText(Forgot.this, "修改成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(Forgot.this, "该用户不存在！", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    Toast.makeText(Forgot.this, "修改失败，请输入你的用户名或密码", Toast.LENGTH_SHORT).show();
                }
            }
        });
        teacha = findViewById(R.id.btn_tealog);
        teacha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String b1 = change1.getText().toString().trim();
                String b2 = change2.getText().toString().trim();
                String b3 = change3.getText().toString().trim();
                if (!TextUtils.isEmpty(b1) && !TextUtils.isEmpty(b2)) {
                    if (!b2.equals(b3)) {
                        Toast.makeText(Forgot.this, "两次输入密码不一致！", Toast.LENGTH_SHORT).show();
                    } else {
                        int a = logTeaDBHelper.update(change1.getText().toString(), change2.getText().toString());
                        if (a > 0) {
                            Toast.makeText(Forgot.this, "修改成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(Forgot.this, "该用户不存在！", Toast.LENGTH_SHORT).show();
                        }

                    }
                } else {
                    Toast.makeText(Forgot.this, "修改失败，请输入你的用户名或密码", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void back() {
        Intent intent = new Intent(this, LogIn.class);
        startActivity(intent);
    }
}
