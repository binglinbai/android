package cn.itcast.redemption;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class LogStuDBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "student_log.db";
    private static final int VERSION = 1;
    private Context context;

    public LogStuDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public LogStuDBHelper(Context context) {
        this(context, DB_NAME, null, VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE infomation(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "xuehao VARCHAR(20),mima VARCHAR(50))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
    }

    public long insert(String xuehao, String mima) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("xuehao", xuehao);
        values.put("mima", mima);
        long id = db.insert("infomation", null, values);
        Toast.makeText(context, "注册成功", Toast.LENGTH_SHORT).show();
        return id;
    }

    public int update(String xuehao, String mima) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mima", mima);
        int number = db.update("infomation", values, "xuehao=?", new String[]{xuehao});
        return number;
    }

    public ArrayList<LogStudent> getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<LogStudent> list = new ArrayList<LogStudent>();
        Cursor cursor = db.query("infomation", null, null, null, null, null, "xuehao DESC");
        while (cursor.moveToNext()) {
            String xuehao = cursor.getString(cursor.getColumnIndex("xuehao"));
            String mima = cursor.getString(cursor.getColumnIndex("mima"));
            list.add(new LogStudent(xuehao, mima));
        }
        return list;
    }

    public boolean norepeat(String xuehao) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query("infomation", null, "xuehao=?", new String[]{xuehao}, null, null, null);
        if (cursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }
}
