package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class About extends Activity implements View.OnClickListener {
    private Button button;
    private Button made;
    private Button fb;
    private ImageView imageView;
    private ImageView iv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        made = findViewById(R.id.made);
        fb = findViewById(R.id.feedback);
        made.setOnClickListener(this);
        fb.setOnClickListener(this);
        iv1 = findViewById(R.id.iv1);
        iv1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == made) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("MADE BY:");
            dialog.setMessage("17物联网一班 刘志远 韩雪冬\n17软工三班  刘帅东 张东升");
            dialog.setCancelable(true);
            dialog.show();
        } else if (v == fb) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);

            dialog.setTitle("FEEDBACK:");
            dialog.setMessage("如果您在使用中发现任何问题，请发送邮件至：787284978@qq.com");
            dialog.setCancelable(true);
            dialog.show();
        } else if (v == iv1) {
            Toast.makeText(About.this, "彩蛋...", Toast.LENGTH_SHORT).show();
        }
    }
}
