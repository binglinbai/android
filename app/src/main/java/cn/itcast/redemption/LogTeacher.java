package cn.itcast.redemption;

public class LogTeacher {
    private String gonghao;
    private String mima;

    public LogTeacher(String gonghao, String mima) {
        this.gonghao = gonghao;
        this.mima = mima;
    }

    public String getGonghao() {
        return gonghao;
    }

    public String getMima() {
        return mima;
    }

    public void setGonghao(String gonghao) {
        this.gonghao = gonghao;
    }

    public void setMima(String mima) {
        this.mima = mima;
    }
}
