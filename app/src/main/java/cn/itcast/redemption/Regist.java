package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Regist extends AppCompatActivity implements View.OnClickListener {
    private Button button;
    private Button cancel;
    private Button stureg;
    private Button teareg;
    private LogStuDBHelper logStuDBHelper;
    private LogTeaDBHelper logTeaDBHelper;
    private EditText editText;
    private EditText et_name;
    private EditText et_password;
    private EditText et1_repassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regist);
        setTitle("用户注册");
        logStuDBHelper = new LogStuDBHelper(this);
        logTeaDBHelper = new LogTeaDBHelper(this);
        et_name = findViewById(R.id.et1_name);
        et_password = findViewById(R.id.et1_password);
        et1_repassword = findViewById(R.id.et1_repassword);
        //取消按钮
        cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
        //学生注册
        stureg = findViewById(R.id.btn_stureg);
        stureg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = et_password.getText().toString().trim();
                String b = et1_repassword.getText().toString().trim();
                String c = et_name.getText().toString().trim();
                boolean result;
                if (a.equals(b)) {
                    if (!TextUtils.isEmpty(c) && !TextUtils.isEmpty(a)) {
                        result = logStuDBHelper.norepeat(c);
                        if (result) {
                            Toast.makeText(Regist.this, "账号已存在！", Toast.LENGTH_SHORT).show();
                        } else {
                            logStuDBHelper.insert(et_name.getText().toString(), et_password.getText().toString());
                            finish();
                        }
                    } else {
                        Toast.makeText(Regist.this, "注册失败，请输入用户名或密码！", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Regist.this, "两次输入密码不一致！", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //教师注册
        teareg = findViewById(R.id.btn_teareg);
        teareg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = et_password.getText().toString().trim();
                String b = et1_repassword.getText().toString().trim();
                String c = et_name.getText().toString().trim();
                boolean result;
                if (a.equals(b)) {
                    if (!TextUtils.isEmpty(c) && !TextUtils.isEmpty(a)) {
                        result = logTeaDBHelper.norepeat(c);
                        if (result) {
                            Toast.makeText(Regist.this, "账号已存在！", Toast.LENGTH_SHORT).show();
                        } else {
                            logTeaDBHelper.insert(et_name.getText().toString(), et_password.getText().toString());

                            finish();
                        }
                    } else {
                        Toast.makeText(Regist.this, "注册失败，请输入用户名或密码！", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Regist.this, "两次输入密码不一致！", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void back() {
        Intent intent = new Intent(this, LogIn.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }
}
