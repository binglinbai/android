package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LogIn extends AppCompatActivity implements View.OnClickListener{
    private TextView textView;
    private Button button;
    private EditText editText;
    private TextView reg;
    private TextView forgot;
    private Button stulog;
    private Button tealog;
    private LogStuDBHelper logStuDBHelper;
    private LogTeaDBHelper logTeaDBHelper;
    private EditText dlming;
    private EditText dlmi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);
        logStuDBHelper=new LogStuDBHelper(this);
        logTeaDBHelper=new LogTeaDBHelper(this);
        setTitle("登陆页面");
        dlming = findViewById(R.id.et_name);
        dlmi = findViewById(R.id.et_password);
        //注册按钮
        reg = findViewById(R.id.tv_reg);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadreg();
            }
        });
        //忘记密码按钮
        forgot = findViewById(R.id.tv_forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadfor();
            }
        });
        //学生登陆按钮
        stulog = findViewById(R.id.btn_stulog);
        stulog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a1=dlming.getText().toString().trim();
                String b1=dlmi.getText().toString().trim();
                if (!TextUtils.isEmpty(a1)&&!TextUtils.isEmpty(b1)){
                    ArrayList<LogStudent> data=logStuDBHelper.getAllData();
                    boolean match=false;
                    for(int i=0;i<data.size();i++){
                        LogStudent logStudent=data.get(i);
                        if(a1.equals(logStudent.getXuehao())&&b1.equals(logStudent.getMima())){
                            match=true;
                            break;
                        }else {
                            match=false;
                        }
                    }
                    if (match){
                        Toast.makeText(LogIn.this, "登录成功", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LogIn.this,StuCheck.class);
                        intent.putExtra("xuehao",dlming.getText().toString().trim());
                        intent.putExtra("mima",dlmi.getText().toString().trim());
                        startActivity(intent);
                    }else {
                        Toast.makeText(LogIn.this, "用户名或密码不正确，请重新输入", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LogIn.this, "请输入你的用户名或密码", Toast.LENGTH_SHORT).show();
                }


            }
        });
        //教师登陆按钮
        tealog = findViewById(R.id.btn_tealog);
        tealog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a1=dlming.getText().toString().trim();
                String b1=dlmi.getText().toString().trim();
                if (!TextUtils.isEmpty(a1)&&!TextUtils.isEmpty(b1)){
                    ArrayList<LogTeacher> data=logTeaDBHelper.getAllData();
                    boolean match=false;
                    for(int i=0;i<data.size();i++){
                        LogTeacher logTeacher=data.get(i);
                        if(a1.equals(logTeacher.getGonghao())&&b1.equals(logTeacher.getMima())){
                            match=true;
                            break;
                        }else {
                            match=false;
                        }
                    }
                    if (match){
                        Toast.makeText(LogIn.this, "登录成功", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LogIn.this,StudentListActivity.class);
                        intent.putExtra("xuehao",dlming.getText().toString().trim());
                        intent.putExtra("mima",dlmi.getText().toString().trim());
                        startActivity(intent);
                    }else {
                        Toast.makeText(LogIn.this, "用户名或密码不正确，请重新输入", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(LogIn.this, "请输入你的用户名或密码", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void loadreg(){
        Intent intent=new Intent(this,Regist.class);
        startActivity(intent);
    }

    public void loadfor(){
        Intent intent=new Intent(this,Forgot.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }
}
