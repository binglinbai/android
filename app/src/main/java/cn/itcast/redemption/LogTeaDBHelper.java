package cn.itcast.redemption;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import cn.itcast.redemption.LogStudent;

public class LogTeaDBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "teacher_log.db";
    private static final int VERSION = 1;
    private Context context;

    public LogTeaDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public LogTeaDBHelper(Context context) {
        this(context, DB_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE teacherinfo(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "gonghao VARCHAR(20),mima VARCHAR(50))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insert(String gonghao, String mima) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("gonghao", gonghao);
        values.put("mima", mima);
        long id = db.insert("teacherinfo", null, values);
        Toast.makeText(context, "注册成功", Toast.LENGTH_SHORT).show();
        return id;
    }

    public int update(String gonghao, String mima) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mima", mima);
        int number = db.update("teacherinfo", values, "gonghao=?", new String[]{gonghao});
        return number;
    }

    public ArrayList<LogTeacher> getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<LogTeacher> list = new ArrayList<LogTeacher>();
        Cursor cursor = db.query("teacherinfo", null, null, null, null, null, "gonghao DESC");
        while (cursor.moveToNext()) {
            String gonghao = cursor.getString(cursor.getColumnIndex("gonghao"));
            String mima = cursor.getString(cursor.getColumnIndex("mima"));
            list.add(new LogTeacher(gonghao, mima));
        }
        return list;
    }

    public boolean norepeat(String gonghao) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query("teacherinfo", null, "gonghao=?", new String[]{gonghao}, null, null, null);
        if (cursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }
}
