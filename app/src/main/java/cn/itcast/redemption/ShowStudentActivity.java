package cn.itcast.redemption;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

//展示单条记录
public class ShowStudentActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_info);
        Intent intent = getIntent();
        Student student = (Student) intent.getSerializableExtra(TableContanst.STUDENT_TABLE);
        ((TextView) findViewById(R.id.tv_info_id)).setText(student.getId() + "");
        ((TextView) findViewById(R.id.tv_info_name)).setText(student.getName());
        ((TextView) findViewById(R.id.tv_info_age)).setText(student.getAge() + "");
        ((TextView) findViewById(R.id.tv_info_sex)).setText(student.getSex());
        ((TextView) findViewById(R.id.tv_info_likes)).setText(student.getLike());
        ((TextView) findViewById(R.id.tv_info_ch)).setText(student.getCh() + "");
        ((TextView) findViewById(R.id.tv_info_da)).setText(student.getDa() + "");
        ((TextView) findViewById(R.id.tv_info_en)).setText(student.getEn() + "");
        ((TextView) findViewById(R.id.tv_info_comp)).setText(student.getComp() + "");
        ((TextView) findViewById(R.id.tv_info_train_date)).setText(student.getTrainDate());
        ((TextView) findViewById(R.id.tv_info_phone)).setText(student.getPhoneNumber());
    }

    public void goBack(View view) {
        finish();
    }

    public void goUpdate(View view) {
        Intent intent = getIntent();
        Student student = (Student) intent.getSerializableExtra(TableContanst.STUDENT_TABLE);
        intent.putExtra("TableContanst.STUDENT_TABLE", student);
        intent.setClass(this, AddStudentActivity.class);
        this.startActivity(intent);
        finish();

    }
}
