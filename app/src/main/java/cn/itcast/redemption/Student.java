package cn.itcast.redemption;

import java.io.Serializable;

public class Student implements Serializable {
    private long id;
    private String name;
    private int age;
    private String sex;
    private String like;
    private String phoneNumber;
    private String trainDate;
    private String modifyDateTime;
    private int ch;
    private int da;
    private int en;
    private int comp;


    public Student() {
        super();
    }

    public Student(long id, String name, int age, String sex, String like, String phoneNumber, String trainDate, String modifyDateTime) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.like = like;
        this.phoneNumber = phoneNumber;
        this.trainDate = trainDate;
        this.modifyDateTime = modifyDateTime;
    }

    public Student(long id, String name, int age, String sex, String like, String phoneNumber, String trainDate, int ch, int da, int en, int comp) {

        super();
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.like = like;
        this.ch = ch;
        this.da = da;
        this.en = en;
        this.comp = comp;
        this.phoneNumber = phoneNumber;
        this.trainDate = trainDate;
    }

    //新增
    public Student(long id, String name, int age, String sex, String like, int ch, int da, int en, int comp, String phoneNumber, String trainDate, String modifyDateTime) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.like = like;
        this.ch = ch;
        this.en = en;
        this.da = da;
        this.comp = comp;
        this.phoneNumber = phoneNumber;
        this.trainDate = trainDate;
        this.modifyDateTime = modifyDateTime;

    }

    public Student(String name, int age, String sex, String like, int ch, int en, int da, int comp, String phoneNumber, String trainDate, String modifyDateTime) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.like = like;
        this.ch = ch;
        this.en = en;
        this.da = da;
        this.comp = comp;
        this.phoneNumber = phoneNumber;
        this.trainDate = trainDate;
        this.modifyDateTime = modifyDateTime;

    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getSex() {
        return sex;
    }

    public String getLike() {
        return like;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTrainDate() {
        return trainDate;
    }

    public String getModifyDateTime() {
        return modifyDateTime;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setTrainDate(String trainDate) {
        this.trainDate = trainDate;
    }

    public void setModifyDateTime(String modifyDateTime) {
        this.modifyDateTime = modifyDateTime;
    }

    //新增

    public int getCh() {
        return ch;
    }

    public int getDa() {
        return da;
    }

    public int getEn() {
        return en;
    }

    public int getComp() {
        return comp;
    }

    public void setCh(int ch) {
        this.ch = ch;
    }

    public void setDa(int da) {
        this.da = da;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public void setComp(int comp) {
        this.comp = comp;
    }
}
