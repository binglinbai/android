package cn.itcast.redemption;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddStudentActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "AddStudentActivity";
    private final static int DATE_DIALOG = 1;
    private static final int DATE_PICKER_ID = 1;
    private TextView idText;
    private EditText nameText;
    private EditText ageText;
    private EditText phoneText;
    private EditText dataText;
    //新增
    private EditText chText;
    private EditText enText;
    private EditText daText;
    private EditText compText;

    private RadioGroup group;
    private RadioButton button1;
    private RadioButton button2;
    private CheckBox box1;
    private CheckBox box2;
    private CheckBox box3;
    private Button restoreButton;
    private String sex;
    private Button resetButton;
    private Long student_id;
    private StudentDao dao;
    private boolean isAdd = true;
    private RadioGroup stata;

    private RadioButton pass;
    private RadioButton failed;
    private RadioButton other;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_student);
        idText = (TextView) findViewById(R.id.tv_stu_id);
        nameText = (EditText) findViewById(R.id.et_name);
        ageText = (EditText) findViewById(R.id.et_age);
        //新增
        chText = (EditText) findViewById(R.id.et_ch);
        enText = (EditText) findViewById(R.id.et_en);
        daText = (EditText) findViewById(R.id.et_da);
        compText = (EditText) findViewById(R.id.et_comp);


        button1 = (RadioButton) findViewById(R.id.rb_sex_female);
        button2 = (RadioButton) findViewById(R.id.rb_sex_male);
        phoneText = (EditText) findViewById(R.id.et_phone);
        dataText = (EditText) findViewById(R.id.et_traindate);
        //又新增
        pass = findViewById(R.id.rb_state_pass);
        failed = findViewById(R.id.rb_state_failed);
        other = findViewById(R.id.rb_state_other);
        group = (RadioGroup) findViewById(R.id.rg_sex);
//        box1 = (CheckBox) findViewById(R.id.box1);
//        box2 = (CheckBox) findViewById(R.id.box2);
//        box3 = (CheckBox) findViewById(R.id.box3);
        stata = findViewById(R.id.rg_state);
        restoreButton = (Button) findViewById(R.id.btn_save);
        resetButton = (Button) findViewById(R.id.btn_clear);
        dao = new StudentDao(new StudentDBHelper(this)); // 设置监听 78
        restoreButton.setOnClickListener(this);
        resetButton.setOnClickListener(this);
        dataText.setOnClickListener(this);
        checkIsAddStudent();
    }

    // 检查此时Activity是否用于添加学员信息
    //此处已经写好了修改信息的方法
    private void checkIsAddStudent() {
        Intent intent = getIntent();
        Serializable serial = intent.getSerializableExtra(TableContanst.STUDENT_TABLE);
        if (serial == null) {
            isAdd = true;
            dataText.setText(getCurrentDate());
        } else {

            isAdd = false;
            Student s = (Student) serial;
            showEditUI(s);
        }
    }

    //显示学员信息更新的UI104
    //更新ui前的还原操作
    private void showEditUI(Student student) {
        // 先将Student携带的数据还原到student的每一个属性中去
        student_id = student.getId();
        String name = student.getName();
        int age = student.getAge();
        String phone = student.getPhoneNumber();
        String data = student.getTrainDate();
        String like = student.getLike();
        String sex = student.getSex();
        //新增
        int ch = student.getCh();
        int da = student.getDa();
        int en = student.getEn();
        int comp = student.getComp();

        //性别按钮的逻辑
        if (sex.toString().equals("男")) {
            button2.setChecked(true);
        } else if (sex.toString().equals("女")) {
            button1.setChecked(true);
        }
        //状态按钮的逻辑
        if (like.toString().equals("通过")) {
            pass.setChecked(true);
        } else if (like.toString().equals("挂科")) {
            failed.setChecked(true);
        } else if (like.toString().equals("其它")) {
            other.setChecked(true);
        }

        // 还原数据
        idText.setText(student_id + "");
        nameText.setText(name + "");
        ageText.setText(age + "");
        phoneText.setText(phone + "");
        dataText.setText(data + "");

        restoreButton.setText("更新");
        //新增
        chText.setText(ch + "");
        daText.setText(da + "");
        enText.setText(en + "");
        compText.setText(comp + "");

    }

    public void onClick(View v) {
        // 收集数据
        if (v == restoreButton) {
            if (!checkUIInput()) {// 界面输入验证
                return;
            }
            Student student = getStudentFromUI();
            if (isAdd) {
                long id = dao.addStudent(student);
                dao.closeDB();
                if (id > 0) {
                    Toast.makeText(this, "保存成功， ID=" + id, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "保存失败，请重新输入！", Toast.LENGTH_SHORT).show();
                }
            } else if (!isAdd) {
                long id = dao.updateStudent(student);
                dao.closeDB();
                if (id > 0) {
                    Toast.makeText(this, "更新成功", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "更新失败，请重新输入！", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (v == resetButton) {
            clearUIData();
        } else if (v == dataText) {
            showDialog(DATE_PICKER_ID);
        }
    }

    //      收集界面输入的数据，并将封装成Student对象
    private Student getStudentFromUI() {
        String name = nameText.getText().toString();
        int age = Integer.parseInt(ageText.getText().toString());
        String sex = ((RadioButton) findViewById(group
                .getCheckedRadioButtonId())).getText().toString();
        String likes = ((RadioButton) findViewById(stata
                .getCheckedRadioButtonId())).getText().toString();
        int ch = Integer.parseInt(chText.getText().toString());
        int da = Integer.parseInt(daText.getText().toString());
        int en = Integer.parseInt(enText.getText().toString());
        int comp = Integer.parseInt(compText.getText().toString());
        String trainDate = dataText.getText().toString();
        String phoneNumber = phoneText.getText().toString();
        String modifyDateTime = getCurrentDateTime();


        Student s = new Student(name, age, sex, likes, ch, en, da, comp, phoneNumber, trainDate,
                modifyDateTime);
        //如果处于更新操作
        if (!isAdd) {
            //学生的id将被设置为当前id
            s.setId(Integer.parseInt(idText.getText().toString()));

        }
        return s;
    }

    //验证用户是否按要求输入了数据
    private boolean checkUIInput() { // name, age, sex
        String name = nameText.getText().toString();
        String age = ageText.getText().toString();
        int id = group.getCheckedRadioButtonId();
        String message = null;
        View invadView = null;
        //新增
        String ch = chText.getText().toString();
        String da = daText.getText().toString();
        String en = enText.getText().toString();
        String comp = compText.getText().toString();


        if (name.trim().length() == 0) {
            message = "请输入姓名！";
            invadView = nameText;
        } else if (age.trim().length() == 0) {
            message = "请输入年龄！";
            invadView = ageText;
        } else if (id == -1) {
            message = "请选择性别！";
        } else if (ch.trim().length() == 0) {
            message = "请输入语文成绩";
            invadView = chText;
        } else if (da.trim().length() == 0) {
            message = "请输入数据结构成绩";
            invadView = daText;
        } else if (en.trim().length() == 0) {
            message = "请输入英语成绩";
            invadView = enText;
        } else if (comp.trim().length() == 0) {
            message = "请输入计算机科学成绩";
            invadView = compText;
        }

        //如果出现错误，message不为空，则使用Toast通知
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            if (invadView != null)
                invadView.requestFocus();
            return false;
        }
        return true;
    }


    //时间的监听与事件
    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dataText.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
        }
    };

    //      * 得到当前的日期时间
    private String getCurrentDateTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return format.format(new Date());
    }

    //      * 得到当前的日期
    private String getCurrentDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(new Date());
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                return new DatePickerDialog(this, onDateSetListener, 2011, 8, 14);
        }
        return null;
    }


    //       重置按钮的逻辑
    private void clearUIData() {
        nameText.setText("");
        ageText.setText("");
        phoneText.setText("");
        dataText.setText("");
//        box1.setChecked(false);
//        box2.setChecked(false);
        group.clearCheck();
        stata.clearCheck();
        //新增
        chText.setText("");
        daText.setText("");
        enText.setText("");
        compText.setText("");
    }

}
